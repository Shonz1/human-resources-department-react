import {Button, Table} from "react-bootstrap";
import {useHistory} from "react-router";
import {useEffect, useState} from "react";
import {apiCall} from "../api";

export const CompanyListPage = () => {
    const history = useHistory();

    const [companyList, setCompanyList] = useState([]);

    const loadData = () => {
        apiCall('/company/findAllCompanies')
            .then(([list]) => setCompanyList(list))
            .catch(err => console.error(err));
    }

    useEffect(() => {
        loadData();
    }, []);

    return (
        <>
            <Button className={'float-right mb-2'} onClick={() => history.push('/company/add')}>Додати</Button>

            <Table striped={true} responsive={true}>
                <thead>
                <tr>
                    <td>ЄДРПОУ</td>
                    <td>Коротка назва</td>
                    <td>Повна назва</td>
                    <td>Дії</td>
                </tr>
                </thead>
                <tbody>
                {companyList.map((i: any) => (
                    <tr key={i.id}>
                        <td>{i.id}</td>
                        <td>{i.shortName}</td>
                        <td>{i.fullName}</td>
                        <td>
                            <i onClick={() => history.push('/company/' + i.id)}>
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 383.947 383.947" width={'1em'} style={{cursor: "pointer"}}>
                                    <polygon points="0,303.947 0,383.947 80,383.947 316.053,147.893 236.053,67.893"/>
                                    <path d="M377.707,56.053L327.893,6.24c-8.32-8.32-21.867-8.32-30.187,0l-39.04,39.04l80,80l39.04-39.04 C386.027,77.92,386.027,64.373,377.707,56.053z"/>
                                </svg>
                            </i>
                        </td>
                    </tr>
                ))}
                </tbody>
            </Table>
        </>
    );
};
