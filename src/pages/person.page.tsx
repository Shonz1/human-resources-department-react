import {Alert, Button, Col, Form, Row} from "react-bootstrap";
import {useEffect, useState} from "react";
import {apiCall} from "../api";
import {useHistory, useParams} from "react-router";

export const PersonPage = () => {
    const params: any = useParams();
    const history = useHistory();

    const [loaded, setLoaded] = useState(false);
    const [error, setError] = useState('');
    const [isPassportEdit, setIsPassportEdit] = useState(false);

    const [id, setId] = useState('');
    const [lastName, setLastName] = useState('');
    const [firstName, setFirstName] = useState('');
    const [patronymic, setPatronymic] = useState('');
    const [gender, setGender] = useState(true);
    const [birthday, setBirthday] = useState(new Date());
    const [education, setEducation]: [any[], any] = useState([]);
    const [currentEducationName, setCurrentEducationName] = useState('');
    const [currentEducationLevel, setCurrentEducationLevel] = useState(0);
    const [passportSerial, setPassportSerial] = useState('');
    const [passportNumber, setPassportNumber] = useState('');
    const [passportIssuer, setPassportIssuer] = useState('');
    const [passportIssuedAt, setPassportIssuedAt] = useState(new Date());

    const loadPerson = async () => {
        const [person] = await apiCall('/person/findPersonById', params.id);
        if (!person)
            return null;

        setLoaded(true);
        setId(person.id);
        setLastName(person.lastName);
        setFirstName(person.firstName);
        setPatronymic(person.patronymic);
        setGender(person.gender);
        setBirthday(person.birthday);

        return person;
    };

    const loadEducation = async (personId: string) => {
        const [education] = await apiCall('/education/findEducationsByPersonId', personId);
        setEducation(education);
        return education;
    };

    const loadPassport = async (personId: string) => {
        const [passports] = await apiCall('/passport/findPassportsByPersonId', personId);
        const passport: any = passports.reverse().sort((a: any, b: any) => (b.issuedAt.valueOf() - a.issuedAt.valueOf()))[0];
        if (passport) {
            setPassportSerial(passport.serial);
            setPassportNumber(passport.number);
            setPassportIssuer(passport.issuer);
            setPassportIssuedAt(passport.issuedAt);
        }
        return passport;
    };

    useEffect(() => {
        if (params.id) {
            loadPerson()
                .then(async person => {
                    if (!person)
                        return;

                    await loadEducation(person.id);
                    await loadPassport(person.id);
                })
                .catch(err => setError(err));
        }
    }, []);

    const getEducation = () => {
        return education.map((i: any, index: number) => (
            <div key={index} style={{marginBottom: '1em', padding: '1em', borderStyle: 'solid', borderColor: '#bababa', borderWidth: '1px', borderRadius: '1em'}}>
                <Form.Group as={Row}>
                    <Form.Label column sm={2}>Назва</Form.Label>
                    <Col sm={10}>
                        <Form.Control disabled type={'text'} value={i.name} />
                    </Col>
                </Form.Group>
                <Form.Group as={Row}>
                    <Form.Label column sm={2}>Рівень</Form.Label>
                    <Col sm={10}>
                        <Form.Control disabled type={'text'} value={getEducationLevels()[i.level]} />
                    </Col>
                </Form.Group>
                <Button variant={'danger'} style={{display: loaded ? 'none' : 'block'}} onClick={() => setEducation([...education.slice(0, index), ...education.slice(index + 1)])}>Видалити</Button>
            </div>
        ));
    };

    const addEducation = async () => {
        if (!currentEducationName)
            return;

        if (loaded) {
            await apiCall('/education/createEducation', id, currentEducationName, currentEducationLevel);
        }

        setEducation([...education, {name: currentEducationName, level: currentEducationLevel}]);
        setCurrentEducationName('');
        setCurrentEducationLevel(0);
    };

    const savePerson = async () => {
        await apiCall('/person/createPerson', id, lastName, firstName, patronymic, gender, birthday);
    };

    const saveEducation = async () => {
        for (const e of education) {
            await apiCall('/education/createEducation', id, e.name, e.level);
        }
    };

    const savePassport = async () => {
        await apiCall('/passport/createPassport', id, passportSerial, passportNumber, passportIssuer, passportIssuedAt);
    };

    const getEducationLevels = () => {
        return require('../education-levels.json');
    };

    const getErrorMessages = () => {
        return require('../error-messages.json');
    };

    const getErrorMessageTranslate = (key: string) => {
        const messages = getErrorMessages();
        return messages.hasOwnProperty(key) ? messages[key] : 'Невідома помилка';
    };

    const getErrorMessage = () => {
        return (
            error ? <Alert variant={'danger'} onClose={() => setError('')} dismissible>{getErrorMessageTranslate(error)}</Alert> : null
        );
    };

    const save = async () => {
        try {
            await savePerson();
            await saveEducation();
            await savePassport();
            history.push('/people');
        } catch (err) {
            console.error(err);
            setError(err);
        }
    };

    const remove = () => {
        apiCall('/person/removePerson', id)
            .then(() => history.push('/people'))
            .catch(err => setError(err));
    };

    return (
        <Form>
            {getErrorMessage()}

            <h3 className={'mb-3'}>{loaded ? 'Редагування фізичної особи' : 'Нова фізична особа'}</h3>

            <Form.Group as={Row}>
                <Form.Label column sm={2}>ІПН</Form.Label>
                <Col sm={10}>
                    <Form.Control type={'text'} placeholder={'ІПН'} value={id} disabled={loaded} onChange={(e) => setId(e.target.value)} />
                </Col>
            </Form.Group>

            <Form.Group as={Row}>
                <Form.Label column sm={2}>Прізвище</Form.Label>
                <Col sm={10}>
                    <Form.Control type={'text'} placeholder={'Прізвище'} value={lastName} disabled={loaded} onChange={(e) => setLastName(e.target.value)} />
                </Col>
            </Form.Group>

            <Form.Group as={Row}>
                <Form.Label column sm={2}>Ім'я</Form.Label>
                <Col sm={10}>
                    <Form.Control type={'text'} placeholder={"Ім'я"} value={firstName} disabled={loaded} onChange={(e) => setFirstName(e.target.value)} />
                </Col>
            </Form.Group>

            <Form.Group as={Row}>
                <Form.Label column sm={2}>По батькові</Form.Label>
                <Col sm={10}>
                    <Form.Control type={'text'} placeholder={'По батькові'} value={patronymic} disabled={loaded} onChange={(e) => setPatronymic(e.target.value)} />
                </Col>
            </Form.Group>

            <Form.Group as={Row}>
                <Form.Label column sm={2}>Стать</Form.Label>
                <Col sm={10}>
                    <Form.Check type={'radio'} name={'gender'} label={'Чоловік'} disabled={loaded} checked={gender} onChange={(e) => setGender(true)} />
                    <Form.Check type={'radio'} name={'gender'} label={'Жінка'} disabled={loaded} checked={!gender} onChange={(e) => setGender(false)} />
                </Col>
            </Form.Group>

            <Form.Group as={Row}>
                <Form.Label column sm={2}>День народження</Form.Label>
                <Col sm={10}>
                    <Form.Control type={'date'} placeholder={'День народження'} disabled={loaded} value={`${birthday.getFullYear()}-${('0' + (birthday.getMonth() + 1)).slice(-2)}-${('0' + birthday.getDate()).slice(-2)}`} onChange={(e) => setBirthday(new Date(e.target.value))} />
                </Col>
            </Form.Group>

            <Form.Group>
                <Form.Label>Освіта</Form.Label>

                {getEducation()}

                <div style={{marginBottom: '2em', padding: '1em', borderStyle: 'solid', borderColor: '#bababa', borderWidth: '1px', borderRadius: '1em'}}>
                    <Form.Group as={Row}>
                        <Form.Label column sm={2}>Назва</Form.Label>
                        <Col sm={10}>
                            <Form.Control type={'text'} placeholder={'Назва'} value={currentEducationName} onChange={e => setCurrentEducationName(e.target.value)} />
                        </Col>
                    </Form.Group>

                    <Form.Group as={Row}>
                        <Form.Label column sm={2}>Рівень</Form.Label>
                        <Col sm={10}>
                            <Form.Control as={'select'} value={currentEducationLevel.toString()} onChange={e => setCurrentEducationLevel(Number(e.target.value)|0)}>
                                {getEducationLevels().map((i: any, index: number) => (<option key={index} value={index}>{i}</option>))}
                            </Form.Control>
                        </Col>
                    </Form.Group>
                    <Button onClick={() => addEducation()}>Додати</Button>
                </div>
            </Form.Group>

            <Form.Group>
                <Form.Label>Паспортні данні</Form.Label>

                <div style={{marginBottom: '2em', padding: '1em', borderStyle: 'solid', borderColor: '#bababa', borderWidth: '1px', borderRadius: '1em'}}>
                    <Form.Group as={Row}>
                        <Form.Label column sm={2}>Серія</Form.Label>
                        <Col sm={10}>
                            <Form.Control type={'text'} placeholder={'Серія'} disabled={loaded && !isPassportEdit} value={passportSerial} onChange={e => setPassportSerial(e.target.value)} />
                        </Col>
                    </Form.Group>

                    <Form.Group as={Row}>
                        <Form.Label column sm={2}>Номер</Form.Label>
                        <Col sm={10}>
                            <Form.Control type={'text'} placeholder={'Номер'} disabled={loaded && !isPassportEdit} value={passportNumber} onChange={e => setPassportNumber(e.target.value)} />
                        </Col>
                    </Form.Group>

                    <Form.Group as={Row}>
                        <Form.Label column sm={2}>Хто видав</Form.Label>
                        <Col sm={10}>
                            <Form.Control type={'text'} placeholder={'Хто видав'} disabled={loaded && !isPassportEdit} value={passportIssuer} onChange={e => setPassportIssuer(e.target.value)} />
                        </Col>
                    </Form.Group>

                    <Form.Group as={Row}>
                        <Form.Label column sm={2}>Коли видан</Form.Label>
                        <Col sm={10}>
                            <Form.Control type={'date'} placeholder={'Коли видан'} disabled={loaded && !isPassportEdit} value={`${passportIssuedAt.getFullYear()}-${('0' + (passportIssuedAt.getMonth() + 1)).slice(-2)}-${('0' + passportIssuedAt.getDate()).slice(-2)}`} onChange={e => setPassportIssuedAt(new Date(e.target.value))} />
                        </Col>
                    </Form.Group>

                    <Row>
                        <Button className={'ml-3 mr-2'} variant={loaded && isPassportEdit ? 'primary' : 'secondary'} style={{display: loaded ? 'block' : 'none'}} onClick={() => {
                            if (isPassportEdit) {
                                savePassport()
                                    .then(() => setIsPassportEdit(false))
                                    .catch(err => setError(err));
                            } else {
                                setIsPassportEdit(true);
                            }
                        }}>{isPassportEdit ? 'Зберегти' : 'Редагувати'}</Button>
                        <Button variant={'secondary'} style={{display: isPassportEdit ? 'block' : 'none'}} onClick={() => {
                            loadPassport(id)
                                .then(() => setIsPassportEdit(false))
                                .catch(err => setError(err));
                        }}>Відмінити</Button>
                    </Row>
                </div>
            </Form.Group>

            <Button className={'mb-2'} block style={{display: !loaded ? 'block' : 'none'}} onClick={() => save()}>{loaded ? 'Зберегти' : 'Додати'}</Button>
            <Button className={'mb-3'} variant={'danger'} block style={{display: loaded ? 'block' : 'none'}} onClick={() => remove()}>{'Видалити'}</Button>
        </Form>
    );
};
