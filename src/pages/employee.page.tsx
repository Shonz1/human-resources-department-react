import {useHistory, useParams} from "react-router";
import {useEffect, useState} from "react";
import type {ChangeEvent} from "react";
import {apiCall} from "../api";
import {Alert, Button, Col, Form, Row} from "react-bootstrap";

export const EmployeePage = () => {
    const params: any = useParams();
    const history = useHistory();

    const [error, setError] = useState('');

    const [id, setId] = useState('');
    const [person, setPerson]: [any, any] = useState(null);
    const [company, setCompany]: [any, any] = useState(null);
    const [role, setRole] = useState('');
    const [salary, setSalary] = useState(0);
    const [socialPrivilege, setSocialPrivilege] = useState(false);

    const loadEmployee = async () => {
        const [employee] = await apiCall('/employee/findEmployeeById', params.id);
        if (!employee)
            return null;

        setId(employee.id);
        setPerson((await apiCall('/person/findPersonById', employee.personId))[0]);
        setCompany((await apiCall('/company/findCompanyById', employee.companyId))[0]);
        setRole(employee.role);
        setSalary(employee.salary);
        setSocialPrivilege(employee.socialPrivilege);
    };

    useEffect(() => {
        loadEmployee()
            .catch(err => setError(err));
    }, []);

    const getErrorMessages = () => {
        return require('../error-messages.json');
    };

    const getErrorMessageTranslate = (key: string) => {
        const messages = getErrorMessages();
        return messages.hasOwnProperty(key) ? messages[key] : 'Невідома помилка';
    };

    const getErrorMessage = () => {
        return (
            error ? <Alert variant={'danger'} onClose={() => setError('')} dismissible>{getErrorMessageTranslate(error)}</Alert> : null
        );
    };

    const remove = async () => {
        try {
            await apiCall('/employee/removeEmployee', id);
            history.goBack();
        } catch (err) {
            setError(err);
        }
    };

    const onChangeRole = async (e: any) => {
        try {
            const value = e.target.value;
            setRole(value);
            await apiCall('/employee/changeEmployeeRole', id, value);
        } catch (err) {
            setError(err);
        }
    };

    const onChangeSalary = async (e: any) => {
        try {
            let value = Number(e.target.value);
            value = isNaN(value) ? 0 : value;
            setSalary(value);
            await apiCall('/employee/changeEmployeeSalary', id, value);
        } catch (err) {
            setError(err);
        }
    };

    const onChangeSocialPrivilege = async (e: any) => {
        try {
            const value = e.target.checked;
            setSocialPrivilege(value);
            await apiCall('/employee/changeEmployeeSocialPrivilege', id, value);
        } catch (err) {
            setError(err);
        }
    };

    return (
        <Form>
            {getErrorMessage()}

            <h3 className={'mb-3'}>{'Редагування працівника'}</h3>

            <Form.Group as={Row}>
                <Form.Label column sm={2}>ІПН</Form.Label>
                <Col sm={10}>
                    <Form.Control type={'text'} placeholder={'ІПН'} value={person?.id} disabled />
                </Col>
            </Form.Group>

            <Form.Group as={Row}>
                <Form.Label column sm={2}>Прізвище</Form.Label>
                <Col sm={10}>
                    <Form.Control type={'text'} placeholder={'Прізвище'} value={person?.lastName} disabled />
                </Col>
            </Form.Group>

            <Form.Group as={Row}>
                <Form.Label column sm={2}>Ім'я</Form.Label>
                <Col sm={10}>
                    <Form.Control type={'text'} placeholder={"Ім'я"} value={person?.firstName} disabled />
                </Col>
            </Form.Group>

            <Form.Group as={Row}>
                <Form.Label column sm={2}>По батькові</Form.Label>
                <Col sm={10}>
                    <Form.Control type={'text'} placeholder={'По батькові'} value={person?.patronymic} disabled />
                </Col>
            </Form.Group>

            <Form.Group as={Row}>
                <Form.Label column sm={2}>Стать</Form.Label>
                <Col sm={10}>
                    <Form.Check type={'radio'} name={'gender'} label={'Чоловік'} disabled checked={person?.gender} />
                    <Form.Check type={'radio'} name={'gender'} label={'Жінка'} disabled checked={!person?.gender} />
                </Col>
            </Form.Group>

            <Form.Group as={Row}>
                <Form.Label column sm={2}>День народження</Form.Label>
                <Col sm={10}>
                    <Form.Control type={'date'} placeholder={'День народження'} disabled value={`${person?.birthday?.getFullYear()}-${('0' + (person?.birthday?.getMonth() + 1)).slice(-2)}-${('0' + person?.birthday?.getDate()).slice(-2)}`} />
                </Col>
            </Form.Group>

            <Form.Group as={Row}>
                <Form.Label column sm={2}>Посада</Form.Label>
                <Col sm={10}>
                    <Form.Control type={'text'} placeholder={'Посада'} value={role} onChange={onChangeRole} />
                </Col>
            </Form.Group>

            <Form.Group as={Row}>
                <Form.Label column sm={2}>Заробітна плата</Form.Label>
                <Col sm={10}>
                    <Form.Control type={'number'} step={0.01} placeholder={'Посада'} value={salary} onChange={onChangeSalary} />
                </Col>
            </Form.Group>

            <Form.Group as={Row}>
                <Form.Label column sm={2}>Соціальні привілеї</Form.Label>
                <Col sm={10}>
                    <Form.Check type={'checkbox'} checked={socialPrivilege} onChange={onChangeSocialPrivilege} />
                </Col>
            </Form.Group>

            <Button className={'mb-3'} variant={'danger'} block onClick={() => remove()}>Звільнити</Button>
        </Form>
    );
};
