import {useEffect, useState} from "react";
import {apiCall} from "../api";
import {Button, Table} from "react-bootstrap";
import {useHistory} from "react-router";

export const PersonListPage = () => {
    const history = useHistory();

    const [personList, setPersonList] = useState([]);

    const loadData = () => {
        apiCall('/person/findAllPeople')
            .then(([list]) => setPersonList(list))
            .catch(err => console.error(err));
    }

    useEffect(() => {
        loadData();
    }, []);

    return (
        <>
            <Button className={'float-right mb-2'} onClick={() => history.push('/person/add')}>Додати</Button>

            <Table striped={true} responsive={true}>
                <thead>
                <tr>
                    <td>ІПН</td>
                    <td>Прізвище</td>
                    <td>Ім'я</td>
                    <td>По батькові</td>
                    <td>Дії</td>
                </tr>
                </thead>
                <tbody>
                {personList.map((i: any) => (
                    <tr key={i.id}>
                        <td>{i.id}</td>
                        <td>{i.lastName}</td>
                        <td>{i.firstName}</td>
                        <td>{i.patronymic}</td>
                        <td>
                            <i onClick={() => history.push('/person/' + i.id)}>
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 383.947 383.947" width={'1em'} style={{cursor: "pointer"}}>
                                    <polygon points="0,303.947 0,383.947 80,383.947 316.053,147.893 236.053,67.893"/>
                                    <path d="M377.707,56.053L327.893,6.24c-8.32-8.32-21.867-8.32-30.187,0l-39.04,39.04l80,80l39.04-39.04 C386.027,77.92,386.027,64.373,377.707,56.053z"/>
                                </svg>
                            </i>
                        </td>
                    </tr>
                ))}
                </tbody>
            </Table>
        </>
    );
};
