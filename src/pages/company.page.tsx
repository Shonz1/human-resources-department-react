import {useHistory, useParams} from "react-router";
import {useEffect, useState} from "react";
import {apiCall} from "../api";
import {Alert, Button, Col, Form, Row, Table} from "react-bootstrap";

export const CompanyPage = () => {
    const params: any = useParams();
    const history = useHistory();

    const [loaded, setLoaded] = useState(false);
    const [error, setError] = useState('');

    const [id, setId] = useState('');
    const [shortName, setShortName] = useState('');
    const [fullName, setFullName] = useState('');
    const [internationalName, setInternationalName] = useState('');
    const [address, setAddress] = useState('');
    const [phone, setPhone] = useState('');
    const [director, setDirector] = useState('');
    const [employees, setEmployees]: [any[], any] = useState([]);

    const [freePeople, setFreePeople]: [any[], any] = useState([]);
    const [currentPerson, setCurrentPerson] = useState('');
    const [currentRole, setCurrentRole] = useState('');
    const [currentSalary, setCurrentSalary] = useState(0);
    const [currentSocialPrivilege, setCurrentSocialPrivilege] = useState(false);

    const loadCompany = async () => {
        const [company] = await apiCall('/company/findCompanyById', params.id);
        if (!company)
            return;

        setLoaded(true);
        setId(company.id);
        setShortName(company.shortName);
        setFullName(company.fullName);
        setInternationalName(company.internationalName);
        setAddress(company.address);
        setPhone(company.phone);
        setDirector(company.director);

        return company;
    };

    const loadPerson = async (personId: string) => {
        const [person] = await apiCall('/employee/findEmployeeById', personId);
        return person;
    };

    const loadDirector = async (employeeId: string) => {
        const [employee] = await apiCall('/employee/findEmployeeById', employeeId);
        setDirector(employee ?? '');
        return employee;
    };

    const loadEmployees = async (companyId: string) => {
        const [employees] = await apiCall('/employee/findEmployeesByCompanyId', companyId);
        const fullData: any[] = await Promise.all(employees.map(async (employee: any) => {
            const [person] = await apiCall('/person/findPersonById', employee.personId);
            return Object.assign({}, person, employee);
        }));
        setEmployees(fullData);
        return employees;
    };

    const loadFreePeople = async (companyId: string) => {
        const [people] = await apiCall('/person/findAllPeople');
        const freePeople = (await Promise.all(people.map(async (person: any) => {
            const [employee] = await apiCall('/employee/findEmployeeByPersonIdAndCompanyId', person.id, companyId);
            return employee ? null : person;
        }))).filter(i => !!i);
        setFreePeople(freePeople);
        return freePeople;
    }

    useEffect(() => {
        if (params.id) {
            loadCompany()
                .then(async company => {
                    if (!company)
                        return;

                    await loadEmployees(company.id);
                    await loadFreePeople(company.id);
                })
                .catch(err => setError(err));
        }
    }, []);

    const getEmployees = () => {
        if (employees.length > 0) {
            return employees.map((i: any, index: number) => (
                <tr key={i.id}>
                    <td>{i.lastName}</td>
                    <td>{i.firstName}</td>
                    <td>{i.patronymic}</td>
                    <td>{i.role}</td>
                    <td>
                        <i onClick={() => history.push('/employee/' + i.id)}>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 383.947 383.947" width={'1em'}
                                 style={{cursor: "pointer"}}>
                                <polygon points="0,303.947 0,383.947 80,383.947 316.053,147.893 236.053,67.893"/>
                                <path
                                    d="M377.707,56.053L327.893,6.24c-8.32-8.32-21.867-8.32-30.187,0l-39.04,39.04l80,80l39.04-39.04 C386.027,77.92,386.027,64.373,377.707,56.053z"/>
                            </svg>
                        </i>
                    </td>
                </tr>
            ));
        } else {
            return (
                <tr>
                    <td colSpan={5} style={{textAlign: 'center'}}>Пусто</td>
                </tr>
            );
        }
    };

    const getErrorMessages = () => {
        return require('../error-messages.json');
    };

    const getErrorMessageTranslate = (key: string) => {
        const messages = getErrorMessages();
        return messages.hasOwnProperty(key) ? messages[key] : 'Невідома помилка';
    };

    const getErrorMessage = () => {
        return (
            error ? <Alert variant={'danger'} onClose={() => setError('')} dismissible>{getErrorMessageTranslate(error)}</Alert> : null
        );
    };

    const addEmployee = () => {
        return apiCall('/employee/createEmployee', currentPerson, id, currentRole, currentSalary, currentSocialPrivilege)
            .then(async () => {
                setCurrentPerson('');
                setCurrentRole('');
                setCurrentSalary(0);
                setCurrentSocialPrivilege(false);
                await loadEmployees(id);
                await loadFreePeople(id);
            })
            .catch(err => setError(err));
    };

    const save = async () => {
        try {
            await apiCall('/company/createCompany', id, shortName, fullName, internationalName, address, phone);
            history.push('/companies');
        } catch (err) {
            console.error(err);
            setError(err);
        }
    };

    const remove = () => {
        apiCall('/company/removeCompany', id)
            .then(() => history.goBack())
            .catch(err => setError(err));
    };

    const onChangeDirector = async (e: any) => {
        try {
            await apiCall('/company/changeCompanyDirector', id, e.target.value);
        } catch (err) {
            setError(err);
        }
    };

    return (
        <Form>
            {getErrorMessage()}

            <h3 className={'mb-3'}>{loaded ? 'Редагування компанії' : 'Нова компанія'}</h3>

            <Form.Group as={Row}>
                <Form.Label column sm={2}>ЄДРПОУ</Form.Label>
                <Col sm={10}>
                    <Form.Control type={'text'} placeholder={'ЄДРПОУ'} value={id} disabled={loaded} onChange={(e) => setId(e.target.value)} />
                </Col>
            </Form.Group>

            <Form.Group as={Row}>
                <Form.Label column sm={2}>Коротка назва</Form.Label>
                <Col sm={10}>
                    <Form.Control type={'text'} placeholder={'Коротка назва'} value={shortName} disabled={loaded} onChange={(e) => setShortName(e.target.value)} />
                </Col>
            </Form.Group>

            <Form.Group as={Row}>
                <Form.Label column sm={2}>Повна назва</Form.Label>
                <Col sm={10}>
                    <Form.Control type={'text'} placeholder={'Повна назва'} value={fullName} disabled={loaded} onChange={(e) => setFullName(e.target.value)} />
                </Col>
            </Form.Group>

            <Form.Group as={Row}>
                <Form.Label column sm={2}>Міжнародна назва</Form.Label>
                <Col sm={10}>
                    <Form.Control type={'text'} placeholder={'Міжнародна назва'} value={internationalName} disabled={loaded} onChange={(e) => setInternationalName(e.target.value)} />
                </Col>
            </Form.Group>

            <Form.Group as={Row}>
                <Form.Label column sm={2}>Адреса</Form.Label>
                <Col sm={10}>
                    <Form.Control type={'text'} placeholder={'Адреса'} value={address} disabled={loaded} onChange={(e) => setAddress(e.target.value)} />
                </Col>
            </Form.Group>

            <Form.Group as={Row}>
                <Form.Label column sm={2}>Телефон</Form.Label>
                <Col sm={10}>
                    <Form.Control type={'text'} placeholder={'Телефон'} value={phone} disabled={loaded} onChange={(e) => setPhone(e.target.value)} />
                </Col>
            </Form.Group>

            <Form.Group as={Row} style={{display: loaded ? 'flex' : 'none'}}>
                <Form.Label column sm={2}>Директор</Form.Label>
                <Col sm={10}>
                    <Form.Control as={'select'} value={director} onChange={onChangeDirector}>
                        <option value={''} selected={true} disabled>Оберіть директора</option>
                        {employees.map((i: any, index: number) => (<option key={index} value={i.id}>{`${i.lastName} ${i.firstName} ${i.patronymic}`}</option>))}
                    </Form.Control>
                </Col>
            </Form.Group>

            <Form.Group style={{display: loaded ? 'block' : 'none'}}>
                <Form.Label sm={2}>Співробітники</Form.Label>

                <Table>
                    <thead>
                        <tr>
                            <td>Прізвище</td>
                            <td>Ім'я</td>
                            <td>По батькові</td>
                            <td>Посада</td>
                            <td>Дії</td>
                        </tr>
                    </thead>
                    <tbody>
                    {getEmployees()}
                    </tbody>
                </Table>

                <div style={{marginBottom: '2em', padding: '1em', borderStyle: 'solid', borderColor: '#bababa', borderWidth: '1px', borderRadius: '1em'}}>
                    <Form.Group as={Row}>
                        <Form.Label column sm={2}>Фізична особа</Form.Label>
                        <Col sm={10}>
                            <Form.Control as={'select'} value={currentPerson} onChange={e => setCurrentPerson(e.target.value)}>
                                <option disabled value={''}>Оберіть фізичну особу</option>
                                {freePeople.map((i: any, index: number) => (<option key={index} value={i.id}>{`${i.lastName} ${i.firstName} ${i.patronymic}`}</option>))}
                            </Form.Control>
                        </Col>
                    </Form.Group>

                    <Form.Group as={Row}>
                        <Form.Label column sm={2}>Посада</Form.Label>
                        <Col sm={10}>
                            <Form.Control type={'text'} placeholder={'Посада'} value={currentRole} onChange={e => setCurrentRole(e.target.value)} />
                        </Col>
                    </Form.Group>

                    <Form.Group as={Row}>
                        <Form.Label column sm={2}>Заробітна плата</Form.Label>
                        <Col sm={10}>
                            <Form.Control type={'number'} placeholder={'Заробітна плата'} value={currentSalary} onChange={e => setCurrentSalary(Number(e.target.value))} />
                        </Col>
                    </Form.Group>

                    <Form.Group as={Row}>
                        <Form.Label column sm={2}>Соціальні привілеї</Form.Label>
                        <Col sm={10}>
                            <Form.Check type={'checkbox'} placeholder={'Соціальні привілеї'} checked={currentSocialPrivilege} onChange={e => setCurrentSocialPrivilege(e.target.checked)} />
                        </Col>
                    </Form.Group>

                    <Button onClick={() => addEmployee()}>Додати</Button>
                </div>
            </Form.Group>

            <Button className={'mb-2'} block style={{display: !loaded ? 'block' : 'none'}} onClick={() => save()}>{loaded ? 'Зберегти' : 'Додати'}</Button>
            <Button className={'mb-3'} variant={'danger'} block style={{display: loaded ? 'block' : 'none'}} onClick={() => remove()}>{'Видалити'}</Button>
        </Form>
    );
};
