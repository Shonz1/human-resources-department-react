import {ipcRenderer} from "electron";
import type {IpcRendererEvent} from "electron";

const HANDLERS: Map<string, Set<(...args: any[]) => void>> = new Map();

ipcRenderer.on('rpcCall', (event: IpcRendererEvent, method: string, ...args: any[]) => {
    HANDLERS.get(method)?.forEach(handler => {
        try {
            handler.apply(null, args);
        } catch (err) {
            console.error(err);
        }
    });
});

export const rpcHandler = (method: string, handler: (...args: any[]) => void) => {
    let handlers = HANDLERS.get(method);
    if (!handlers) {
        handlers = new Set();
        HANDLERS.set(method, handlers);
    }

    handlers.add(handler);
};

export const rpcCall = (method: string, ...args: any[]) => {
    ipcRenderer.send('rpcCall', method, ...args);
};

export const getData = (...args: any[]): Promise<any> => {
    return new Promise((resolve, reject) => {
        const _id = Math.random();

        const subscribe = () => {
            ipcRenderer.once('getData', (event, id, err, result) => {
                if (id !== _id) {
                    subscribe();
                    return;
                }

                if (err)
                    reject(err);
                else
                    resolve(result);
            });
        }
        subscribe();

        ipcRenderer.send('getData', _id, ...args);
    });
};

const randomUuid = () => 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    const r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
});

export const apiCall = (path: string, ...args: any[]): Promise<any[]> => {
    console.log('Api call', path);
    return new Promise((resolve, reject) => {
        const id = randomUuid();
        const subscribe = () => {
            ipcRenderer.once(path, (event, _id, err, ..._args) => {
                if (_id !== id)
                    return subscribe();

                err ? reject(err) : resolve(_args);
            });
        };
        subscribe();

        ipcRenderer.send(path, id, ...args);
    });
}
