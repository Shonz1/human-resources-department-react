import {Container, Nav, Navbar} from "react-bootstrap";
import {HashRouter, Switch, Route} from "react-router-dom";
import {PersonListPage} from "./pages/person-list.page";
import {PersonPage} from "./pages/person.page";
import {CompanyListPage} from "./pages/company-list.page";
import {CompanyPage} from "./pages/company.page";
import {EmployeePage} from "./pages/employee.page";

export const App = () => {
    return (
        <div className="App">
            <HashRouter>
                <Navbar bg={'dark'}  variant={'dark'} fixed={'top'}>
                    <Nav className={'mr-auto'}>
                        <Nav.Link href="#people">Фізичні особи</Nav.Link>
                        <Nav.Link href="#companies">Компанії</Nav.Link>
                    </Nav>
                </Navbar>

                <Container className={'mt-5 pt-4'}>
                    <Switch>
                        <Route path={'/people'} component={PersonListPage} />
                        <Route path={'/person/add'} component={PersonPage} />
                        <Route path={'/person/:id'} component={PersonPage} />
                        <Route path={'/companies'} component={CompanyListPage} />
                        <Route path={'/company/add'} component={CompanyPage} />
                        <Route path={'/company/:id'} component={CompanyPage} />
                        <Route path={'/employee/:id'} component={EmployeePage} />
                    </Switch>
                </Container>
            </HashRouter>
        </div>
    );
};
